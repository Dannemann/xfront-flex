Environment set up:
===================
### External config.:
* Install Flash player debuggers: **https://www.adobe.com/support/flashplayer/debug_downloads.html**

### Flash Builder preferences:
* Set the correct Flex SDK.

### Optional Flash Builder preferences:
* Set the player debugger to the most recent one (Window->Preferences->Flash Builder->Debug).
* Always launch the previously launched application.
* Set the preferred browser (Chrome will need crossdomain.xml).
* Disable welcome screen and *"Check if Help Documentation is installed in your language on startup"*.


Project set up:
===============
1. Import this project to Flash Builder as a **Flex Library Project** named **xfront-flex**. 
1. Append the compiler options: **-locale=en_US,pt_BR -source-path=locale/{locale} -include-file defaults.css assets/css/defaults.css -keep**
